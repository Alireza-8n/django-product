#!/usr/bin/env bash

echo "Waiting for database..."
while ! nc -z ${DB_HOST} ${DB_PORT}; do sleep 1; done
echo "Connected to database."

python src/manage.py migrate --noinput
if [ $? -ne 0 ]; then
    echo "Migration failed." >&2
    exit 1
fi

if [[ $# -gt 0 ]]; then
    INPUT=$@
    sh -c "$INPUT"
else

    python src/manage.py collectstatic --noinput

    echo "Starting Gunicorn..."
    exec gunicorn project.wsgi:application \
        --name carinspection-service-gunicorn \
        --bind 0.0.0.0:80 \
        --workers 10 \
        --pythonpath "/usr/project/src" \
        --log-level=info \
        --log-file=- \
        --access-logfile=- \
        --error-logfile=- \
        --timeout 100000 \
        --reload
fi
